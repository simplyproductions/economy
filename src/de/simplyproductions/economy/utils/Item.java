package de.simplyproductions.economy.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class Item {
    private ItemStack is;

    public Item(Item i) {
        this.is = i.getItemStack();
    }

    public Item(ItemStack is) {
        this.is = is;
    }

    public Item(Material mat) {
        this.is = new ItemStack(mat);
    }

    public Item(Material mat, Integer amount) {
        this.is = new ItemStack(mat, amount);
    }

    public Item(Material mat, Integer amount, Integer subID) {
        this.is = new ItemStack(mat, amount, Short.valueOf(String.valueOf(subID)));
    }

    public Item setMaterial(Material mat) {
        this.is.setType(mat);
        return this;
    }

    public Item setAmount(Integer amount) {
        this.is.setAmount(amount);
        return this;
    }

    @SuppressWarnings("deprecation")
    public Item setDurability(Integer durability) {
        this.is.setDurability(Short.parseShort(String.valueOf(durability)));
        return this;
    }

    public Item setName(String name) {
        ItemMeta m = this.is.getItemMeta();
        m.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        this.is.setItemMeta(m);
        return this;
    }

    public Item setLore(List<String> lore) {
        ItemMeta m = this.is.getItemMeta();
        m.setLore(lore);
        this.is.setItemMeta(m);
        return this;
    }

    public Item addLoreLine(String text) {
        ItemMeta m = this.is.getItemMeta();
        List<String> lore = m.getLore() != null ? m.getLore() : new ArrayList();
        lore.add(text);
        m.setLore(lore);
        this.is.setItemMeta(m);
        return this;
    }

    public Item enchant(Enchantment enchantment, Integer level) {
        this.is.addUnsafeEnchantment(enchantment, level);
        return this;
    }

    public List<String> getLore() {
        return this.is.getItemMeta().getLore();
    }

    public ItemStack getItemStack() {
        return this.is;
    }

    public boolean compare(Item item, boolean ignoreAmount) {
        boolean same = true;

        if(item.getMaterial() != this.getMaterial()) {
            same = false;
        }

        if((item.getAmount() != this.getAmount()) && !ignoreAmount) {
            same = false;
        }

        if(item.getItemStack().getItemMeta() != this.getItemStack().getItemMeta()) {
            same = false;
        }

        return same;
    }

    public Material getMaterial() {
        return this.is.getType();
    }

    public Integer getAmount() {
        return this.is.getAmount();
    }

    public Item setUnbreakable(boolean unbreakable) {
        ItemMeta m = this.is.getItemMeta();
        m.spigot().setUnbreakable(unbreakable);
        this.is.setItemMeta(m);
        return this;
    }

    public Item addItemFlags(ItemFlag... flags) {
        ItemMeta m = this.is.getItemMeta();
        m.addItemFlags(flags);
        this.is.setItemMeta(m);
        return this;
    }

    @SuppressWarnings("deprecation")
    public Item setSkullOwner(String owner) {
        if (this.is.getType() == Material.SKULL_ITEM) {
            SkullMeta sm = (SkullMeta) this.is.getItemMeta();
            sm.setOwner(owner);
            this.is.setItemMeta(sm);
        }

        return this;
    }

    public Item clone() {
        return new Item(this);
    }

    public Item setLeatherColor(Color color) {
        LeatherArmorMeta lam = (LeatherArmorMeta) this.is.getItemMeta();
        lam.setColor(color);
        this.is.setItemMeta(lam);
        return this;
    }

    public String getName() {
        return this.is.getItemMeta().getDisplayName();
    }

    public Item clearLore() {
        ItemMeta im = this.is.getItemMeta();
        im.setLore(new ArrayList());
        this.is.setItemMeta(im);
        return this;
    }

}