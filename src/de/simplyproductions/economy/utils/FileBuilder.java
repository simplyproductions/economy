package de.simplyproductions.economy.utils;

import java.io.File;
import java.io.IOException;

class FileBuilder {
    private File file;
    private FolderBuilder folder;

    /**
     * Create a new instance of {@link FileBuilder}.
     *
     * @param file the file which'll be represented by this instance
     */
    FileBuilder(File file) {
        this.file = file;
        this.folder = new FolderBuilder(file.getParent());
    }

    /**
     * Create a new instance of {@link FileBuilder}.
     *
     * @param folder   the folder of the file which'll be represented by this instance
     * @param filename the file which'll be represented by this instance
     */
    FileBuilder(String folder, String filename) {
        this.folder = new FolderBuilder(folder);
        this.file = new File(this.folder.getPath(), filename);
    }

    /**
     * Create a new instance of {@link FileBuilder}.
     *
     * @param pathWfilename the path of the file which'll be represented by this instance
     */
    FileBuilder(String pathWfilename) {
        this.folder = new FolderBuilder(new File(pathWfilename).getParentFile().getPath());
        this.file = new File(pathWfilename);
    }

    /**
     * Create a new instance of {@link FileBuilder}.
     *
     * @param folder   the folder of the file which'll be represented by this instance
     * @param filename the file which'll be represented by this instance
     */
    FileBuilder(FolderBuilder folder, String filename) {
        this.folder = folder;
        this.file = new File(folder.getPath(), filename);
    }

    /**
     * Check if the file exists.
     *
     * @return if the file exists
     */
    boolean exists() {
        return this.file.exists();
    }

    /**
     * Creates the file if it doesn't exists.
     *
     * @return if the file was successfully created
     */
    boolean create() {
        if (!this.exists() && !this.folder.exists()) {
            this.folder.create();

            try {
                this.file.createNewFile();
                return true;
            } catch (IOException var2) {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Deletes the file if it exists.
     *
     * @return if the file was successfully deleted
     */
    boolean delete() {
        return this.file.delete();
    }

    /**
     * Get the file which is represented by this instance.
     *
     * @return the file
     */
    File getFile() {
        return this.file;
    }

    /**
     * Get the {@link ConfigEditor} instance for editing this instance.
     *
     * @return the {@link ConfigEditor}
     */
    ConfigEditor getConfig() {
        return new ConfigEditor(this);
    }

}