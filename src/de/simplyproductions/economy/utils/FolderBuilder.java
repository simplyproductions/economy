package de.simplyproductions.economy.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

 class FolderBuilder {

    private String path;

    /**
     * Create a new instance of {@link FolderBuilder}.
     *
     * @param path of the folder which'll be represented by this instance
     */
     FolderBuilder(String path) {
        this.path = path;
    }

    /**
     * Get the path of the folder which is represented by this instance.
     *
     * @return the path
     */
     String getPath() {
        return this.path;
    }

    /**
     * Creates the folder if it doesn't exists.
     *
     * @return if the folder was successfully created
     */
     boolean create() {
        File folder = new File(this.path);
        if (folder.isDirectory() && !folder.exists()) {
            folder.mkdirs();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get if the folder exists.
     *
     * @return if the folder exists
     */
     boolean exists() {
        return (new File(this.path)).exists();
    }

    /**
     * Get all files that're in the folder.
     *
     * @return a list of the files ({@link FileBuilder})
     */
     List<FileBuilder> getFiles() {
        if (!this.exists()) {
            return null;
        } else {
            List<FileBuilder> files = new ArrayList();
            File[] var2 = (new File(this.path)).listFiles();
            int var3 = var2.length;

            for (int var4 = 0; var4 < var3; ++var4) {
                File f = var2[var4];
                files.add(new FileBuilder(f));
            }

            return files;
        }
    }

    /**
     * Get an instance of this folder as a {@link File}.
     *
     * @return the folder as a {@link File}
     */
     File toFile() {
        return new File(this.path);
    }

}