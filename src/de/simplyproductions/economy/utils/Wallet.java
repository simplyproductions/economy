package de.simplyproductions.economy.utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.List;

public class Wallet {

    public static Item getSoonIcon(String feature) {
        return new Item(Material.BARRIER)
                .setName("§a" + feature + " §f[§4NOT AVIABLE§f]")
                .addLoreLine("§8---------------")
                .addLoreLine("")
                .addLoreLine("§cThis feature will be added")
                .addLoreLine("§cin an upcoming update.");
    }

    public static Item getPlaceholderItem() {
        return new Item(Material.STAINED_GLASS_PANE, 1, 0)
                .setName("§1");
    }

    public static class MainInventoryContent {

        public static Item getPlayerheadIcon(Player viewer, OfflinePlayer player) {
            Item head = new Item(Material.SKULL_ITEM, 1, 3)
                    .setSkullOwner(player.getName())
                    .setName("§a" + player.getName())
                    .addLoreLine("§8---------------")
                    .addLoreLine("");

            if (viewer.getUniqueId().toString().equals(player.getUniqueId().toString())) {
                // EQUAL PLAYER
                head.addLoreLine("§fYou can use this inventory to")
                        .addLoreLine("§8- §7look up your balance")
                        .addLoreLine("§8- §7do payments")
                        .addLoreLine("§8- §7search recent transactions of yourself");
            } else {
                // OTHER PLAYER
                head.addLoreLine("§7You're currently taking a peek")
                        .addLoreLine("§7at the bank account of §2" + player.getName() + "§7.");
            }

            return head;
        }

        public static Item getBalanceIcon(OfflinePlayer player) {
            EconomyAPI eco = new EconomyAPI(player);
            Integer balance = eco.get();


            return new Item(Material.GOLD_INGOT)
                    .setName("§6" + EconomyAPI.getString(balance));
        }

        public static Item getPaymentIcon() {
            return new Item(Material.EMERALD)
                    .setName("§6Payment");
        }

    }

    public Inventory getMainInventory(Player viewer, OfflinePlayer player) {
        //String title = "§a" + (viewer == player ? "Your Wallet" : "Wallet from " + player.getName());
        Menu menu = new Menu(InventoryType.BREWING, "§aWallet").fill(getPlaceholderItem(), true);

        menu.item(MainInventoryContent.getPlayerheadIcon(viewer, player), ChestSlot.BrewingStand.INPUT);

        if (viewer == player) {
            menu.item(MainInventoryContent.getBalanceIcon(player), ChestSlot.BrewingStand.OUTPUT_LEFT);
            menu.item(MainInventoryContent.getPaymentIcon(), ChestSlot.BrewingStand.OUTPUT_MIDDLE);
            menu.item(getSoonIcon("Transactions"), ChestSlot.BrewingStand.OUTPUT_RIGHT);
        } else {
            menu.item(MainInventoryContent.getBalanceIcon(player), ChestSlot.BrewingStand.OUTPUT_LEFT);
            menu.item(getPlaceholderItem(), ChestSlot.BrewingStand.OUTPUT_MIDDLE);
            menu.item(getSoonIcon("Transactions with this player"), ChestSlot.BrewingStand.OUTPUT_RIGHT);
        }

        return menu.toInventory();
    }

    public Inventory getPaymentInventory(Player viewer) {
        Menu menu = new Menu(3, "§aCreate a Payment")
                .fill(new Item(Material.STAINED_GLASS_PANE, 1, 0), true);
        Payment payment = (Payment.getActivePayment(viewer) != null ? Payment.getActivePayment(viewer) : new Payment(viewer));

        Item recipient, amount, finish;

        recipient = new Item(Material.SKULL_ITEM, 1, 3)
                .setSkullOwner((payment.getRecipient() != null ? payment.getRecipient().getName() : "MHF_Question"))
                .setName("§a" + (payment.getRecipient() != null ? payment.getRecipient().getName() : "Click to choose a player."));

        if (payment.getRecipient() != null) {
            amount = new Item(Material.GOLD_NUGGET)
                    .setName("§a" + (payment.getAmount() != null ? EconomyAPI.getString(payment.getAmount()) : "§aClick to set the amount."));
        } else {
            amount = new Item(Material.BARRIER)
                    .setName("§cPlease choose a player first.");
        }

        if (payment.getAmount() != null) {
            finish = new Item(Material.EMERALD)
                    .setName("§2Complete payment")
                    .addLoreLine("§a")
                    .addLoreLine("§a")
                    .addLoreLine("§fSummary:")
                    .addLoreLine("§a")
                    .addLoreLine("§7» §aRecipient: §2" + payment.getRecipient().getName())
                    .addLoreLine("§7» §aAmount: §2" + EconomyAPI.getString(payment.getAmount()))
                    .addLoreLine("")
                    .addLoreLine("")
                    .addLoreLine("§7● §fClick to confirm this transaction.");
        } else {
            finish = new Item(Material.BARRIER)
                    .setName("§cPlease choose an amount first.");
        }

        menu.item(recipient, ChestSlot.getSlot(2, 3));
        menu.item(amount, ChestSlot.getSlot(2, 5));
        menu.item(finish, ChestSlot.getSlot(2, 7));

        return menu.toInventory();
    }

    public static String getPreviousPageItemName() {
        return "§2« §aPrevious";
    }

    public static String getNextPageItemName() {
        return "§aNext §2»";
    }

    public static Item getBack() {
        return new Item(Material.ARROW)
                .setName("§4« §cBack");
    }

    public static void openPlayerSelection(Player player, Integer page) {
        int invsize = 9 * 5;
        Inventory inv = Bukkit.createInventory(null, invsize, "§aChoose a recipient");
        int itemsPerPage = invsize - (9 * 2);
        List<Player> players = new ArrayList<>();
        Bukkit.getOnlinePlayers().forEach(pl -> {
            if (pl != player) {
                players.add(pl);
            }
        });

        for (int i = ChestSlot.getSlot(4, 1); i <= ChestSlot.getSlot(4, 9); i++) {
            inv.setItem(i, getPlaceholderItem().getItemStack());
        }

        boolean previous = (page > 1);
        boolean next = players.size() >= (itemsPerPage * page);

        Item previousIcon = new Item(Material.STONE)
                .setName(getPreviousPageItemName());

        if (previous) {
            previousIcon.setMaterial(Material.SUGAR);
            previousIcon.addLoreLine("§7● §eClick to change to page " + page + ".");
        } else {
            previousIcon.setMaterial(Material.SULPHUR);
            previousIcon.addLoreLine("§7● §eThere's no previous page.");
        }

        Item nextIcon = new Item(Material.STONE)
                .setName(getNextPageItemName());

        if (next) {
            nextIcon.setMaterial(Material.SUGAR);
            nextIcon.addLoreLine("§7● §eClick to change to page " + page + ".");
        } else {
            nextIcon.setMaterial(Material.SULPHUR);
            nextIcon.addLoreLine("§7● §eThere's no next page.");
        }

        Item pageIcon = new Item(Material.PAPER)
                .setName("§2" + page + ". §aPage");


        inv.setItem(ChestSlot.getSlot((invsize / 9), 8), previousIcon.getItemStack());
        inv.setItem(ChestSlot.getSlot((invsize / 9), 9), nextIcon.getItemStack());

        inv.setItem(ChestSlot.getSlot((invsize / 9), 5), pageIcon.getItemStack());

        inv.setItem(ChestSlot.getSlot((invsize / 9), 1), getBack().getItemStack());


        List<Player> playerselection = (List<Player>) InventoryHelper.getPageContent(players, itemsPerPage, page);

        Item head = new Item(Material.SKULL_ITEM, 1, 3);

        for (Player p : playerselection) {
            head.setSkullOwner(p.getName());
            head.setName("§a" + p.getName());
            head.addLoreLine("§7● §eClick to select " + p.getName());

            inv.addItem(head.getItemStack());
        }


        player.openInventory(inv);
    }

    public Item getAmountMinusOneIcon() {
        return new Item(Material.INK_SACK, 1, 1)
                .setName("§cDecrease amount by one (1)");
    }

    public Item getAmountPlusOneIcon() {
        return new Item(Material.INK_SACK, 1, 10)
                .setName("§aIncrease amount by one (1)");
    }


    public Item getAmountMinusTenIcon() {
        return new Item(Material.WOOL, 1, 14)
                .setName("§cDecrease amount by ten (10)");
    }

    public Item getAmountPlusTenIcon() {
        return new Item(Material.WOOL, 1, 5)
                .setName("§aIncrease amount by ten (10)");
    }


    public Item getAmountMinusHundredIcon() {
        return new Item(Material.STAINED_CLAY, 1, 14)
                .setName("§cDecrease amount by one hundred (100)");
    }

    public Item getAmountPlusHundredIcon() {
        return new Item(Material.STAINED_CLAY, 1, 5)
                .setName("§aIncrease amount by one hundred (100)");
    }


    public Item getAmountMinusThousandIcon() {
        return new Item(Material.REDSTONE_BLOCK)
                .setName("§cDecrease amount by one thousand (1000)");
    }

    public Item getAmountPlusThousandIcon() {
        return new Item(Material.EMERALD_BLOCK)
                .setName("§aIncrease amount by one thousand (1000)");
    }


    public Item getAmountSaveIcon() {
        return new Item(Material.ARROW)
                .setName("§7« §aSave & Back");
    }


    public void openAmountSelection(Player player) {
        Menu menu = new Menu(4, "§aChoose an amount")
                .fill(getPlaceholderItem(), true);
        Payment payment = (Payment.getActivePayment(player) != null ? Payment.getActivePayment(player) : new Payment(player));
        EconomyAPI eco = new EconomyAPI(player);

        if (payment.getAmount() == null) {
            payment.setAmount(1);
            Payment.payments.put(player, payment);
        }

        Item current = new Item(Material.GOLD_INGOT)
                .setName("§6" + EconomyAPI.getString(payment.getAmount()));

        menu.item(current, ChestSlot.getSlot(2, 5));

        if (payment.getAmount() > 1000) {
            menu.item(getAmountMinusThousandIcon(), ChestSlot.getSlot(3, 1));
        }
        if (payment.getAmount() > 100) {
            menu.item(getAmountMinusHundredIcon(), ChestSlot.getSlot(3, 2));
        }
        if (payment.getAmount() > 10) {
            menu.item(getAmountMinusTenIcon(), ChestSlot.getSlot(3, 3));
        }
        if (payment.getAmount() > 1) {
            menu.item(getAmountMinusOneIcon(), ChestSlot.getSlot(3, 4));
        }

        if (eco.has(payment.getAmount() + 1)) {
            menu.item(getAmountPlusOneIcon(), ChestSlot.getSlot(3, 6));
        }
        if (eco.has(payment.getAmount() + 10)) {
            menu.item(getAmountPlusTenIcon(), ChestSlot.getSlot(3, 7));
        }
        if (eco.has(payment.getAmount() + 100)) {
            menu.item(getAmountPlusHundredIcon(), ChestSlot.getSlot(3, 8));
        }
        if (eco.has(payment.getAmount() + 1000)) {
            menu.item(getAmountPlusThousandIcon(), ChestSlot.getSlot(3, 9));
        }

        menu.item(getAmountSaveIcon(), ChestSlot.getSlot(1, 1));

        player.openInventory(menu.toInventory());
    }

}
