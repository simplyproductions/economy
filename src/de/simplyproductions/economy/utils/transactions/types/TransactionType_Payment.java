package de.simplyproductions.economy.utils.transactions.types;

public class TransactionType_Payment implements TransactionType {

    @Override
    public String getName() {
        return "PAYMENT";
    }

    @Override
    public String getDisplayName() {
        return "Payment";
    }

}
