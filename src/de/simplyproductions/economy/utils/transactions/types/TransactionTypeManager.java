package de.simplyproductions.economy.utils.transactions.types;

import java.util.ArrayList;
import java.util.List;

public class TransactionTypeManager {

    public static List<TransactionType> types = new ArrayList<>();

    public static void registerTypes() {
        addType(new TransactionType_Payment());
    }

    /**
     * Add another TransactionType.
     *
     * @param type the new types instance.
     */
    public static void addType(TransactionType type) {
        types.add(type);
    }

    /**
     * Get a {@link TransactionType} by it's configuration name.
     *
     * @param name the configuration name.
     * @return the {@link TransactionType}.
     */
    public static TransactionType getByName(String name) {
        TransactionType type = null;

        for (TransactionType t : types) {
            if (t.getName().equalsIgnoreCase(name)) {
                type = t;
            }
        }

        return type;
    }

}
