package de.simplyproductions.economy.utils.transactions.types;

public interface TransactionType {

    /**
     * The name that will be displayed ingame.
     *
     * @return the ingame name
     */
    String getDisplayName();

    /**
     * The name that will be used for configs
     * (spaces will be removed)
     *
     * @return the configuration name
     */
    String getName();

}