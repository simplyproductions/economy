package de.simplyproductions.economy.utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Menu {

    private Inventory inventory;

    /**
     * Create an instance of type {@link Menu} by an inventory.
     *
     * @param inventory the original inventory.
     */
    public Menu(Inventory inventory) {
        this.inventory = inventory;
    }

    /**
     * Create an instance of type {@link Menu}.
     *
     * @param rows the number of rows.
     * @param title the inventory's title.
     */
    public Menu(Integer rows, String title) {
        inventory = Bukkit.createInventory(null, 9 * rows, title);
    }

    /**
     * Create an instance of type {@link Menu} by an inventory.
     *
     * @param type the type of the inventory.
     */
    public Menu(InventoryType type) {
        inventory = Bukkit.createInventory(null, type);
    }

    /**
     * Create an instance of type {@link Menu} by an inventory.
     *
     * @param type the type of the inventory.
     * @param title the title of the inventory.
     */
    public Menu(InventoryType type, String title) {
        inventory = Bukkit.createInventory(null, type, title);
    }

    /**
     * Set an item into the inventory.
     *
     * @param item the item.
     * @param slot the slot.
     * @return the menu instance.
     */
    public Menu item(Item item, Integer slot) {
        inventory.setItem(slot, item.getItemStack());
        return this;
    }

    /**
     * Fill the complete inventory.
     *
     * @param item the item to fill the inventory with.
     * @param override should already filled slots be overwritten?
     * @return the menu instance.
     */
    public Menu fill(Item item, boolean override) {
        for (int i = 0; i < inventory.getSize(); i++) {
            try {
                if (inventory.getItem(i) == null || inventory.getItem(i).getType() == Material.AIR || override) {
                    inventory.setItem(i, item.getItemStack());
                }
            } catch (NullPointerException e) {
                // SHUT UP, PLEASE.
            }
        }
        return this;
    }

    /**
     * Clear the inventory.
     *
     * @return the menu instance.
     */
    public Menu tidy() {
        inventory.clear();
        return this;
    }

    /**
     * Shuffle the inventory's content.
     *
     * @return the menu instance.
     */
    public Menu shuffle() {
        ItemStack[] items = inventory.getContents();
        List<ItemStack> itemList = Arrays.asList(items);

        Collections.shuffle(itemList);
        itemList.toArray(items);

        inventory.setContents(items);
        return this;
    }

    /**
     * Get the inventory's title.
     *
     * @return the title
     */
    public String getCaption() {
        return inventory.getTitle();
    }

    /**
     * Get how many slots are in the inventory.
     *
     * @return the slots.
     */
    public Integer getSlots() {
        return inventory.getSize();
    }

    /**
     * Get the item from the given slot.
     *
     * @param slot the slot.
     * @return the item.
     */
    public Item getItemOnSlot(int slot) {
        return new Item(inventory.getItem(slot));
    }

    /**
     * Get the bukkit inventory.
     *
     * @return the bukkit inventory.
     */
    public Inventory toInventory() {
        return inventory;
    }

}
