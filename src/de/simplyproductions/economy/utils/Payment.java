package de.simplyproductions.economy.utils;

import de.simplyproductions.economy.listeners.custom.BalanceChangeEvent;
import de.simplyproductions.economy.listeners.custom.TransactionCompletionEvent;
import de.simplyproductions.economy.utils.transactions.types.TransactionType_Payment;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.Date;
import java.util.HashMap;

public class Payment {

    public enum CancellationCause {

        NO_RECIPIENT,
        NO_AMOUNT,
        MISSING_BALANCE;

    }

    public static HashMap<OfflinePlayer, Payment> payments = new HashMap<>();

    public static Payment getActivePayment(OfflinePlayer op) {
        if (payments.containsKey(op)) {
            return payments.get(op);
        }
        return null;
    }

    private OfflinePlayer sender, recipient;
    private Integer amount;
    private EconomyAPI eco;

    public Payment(OfflinePlayer sender) {
        this.sender = sender;
        this.eco = new EconomyAPI(sender);
    }

    public CancellationCause pay() {
        if (this.recipient == null) {
            return CancellationCause.NO_RECIPIENT;
        }
        if (this.amount == null) {
            return CancellationCause.NO_AMOUNT;
        }
        if (!eco.has(amount)) {
            return CancellationCause.MISSING_BALANCE;
        }

        Transaction transaction = new Transaction(sender, recipient, amount, new TransactionType_Payment(), new Date());

        eco.remove(amount, transaction);
        eco.setPlayer(recipient);
        eco.add(amount, transaction);

        TransactionCompletionEvent event = new TransactionCompletionEvent(sender, transaction);
        Bukkit.getPluginManager().callEvent(event);
        return null;
    }

    public static HashMap<OfflinePlayer, Payment> getPayments() {
        return payments;
    }

    public OfflinePlayer getSender() {
        return sender;
    }

    public OfflinePlayer getRecipient() {
        return recipient;
    }

    public void setRecipient(OfflinePlayer recipient) {
        this.recipient = recipient;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

}
