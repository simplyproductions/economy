package de.simplyproductions.economy.utils;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

 class ConfigEditor {

    private File file;
    private YamlConfiguration configuration;

    /**
     * Create a new instance of {@link ConfigEditor}.
     *
     * @param fb the filebuilder instance of the file which'll be represented by this instance
     */
     ConfigEditor(FileBuilder fb) {
        this.file = fb.getFile();
        this.configuration = YamlConfiguration.loadConfiguration(this.file);
    }

    /**
     * Set a value in the config.
     *
     * @param path  the path of the value you want to change
     * @param value the value
     */
     void set(String path, Object value) {
        this.configuration.set(path, value);

        try {
            this.configuration.save(this.file);
        } catch (IOException var4) {
        }

    }

    /**
     * Get a value from the config
     *
     * @param path the path of the value you want to get
     * @return the value
     */
     Object get(String path) {
        return this.configuration.get(path);
    }

    /**
     * Get the file that is edited by this instance.
     *
     * @return the file
     */
     FileBuilder getFile() {
        return new FileBuilder(this.file);
    }

    /**
     * Check if a config contains a path.
     *
     * @param path the path
     * @return if the config contains the path
     */
     boolean contains(String path) {
        return this.configuration.contains(path);
    }
}