package de.simplyproductions.economy.utils;

import de.simplyproductions.economy.listeners.custom.BalanceChangeEvent;
import de.simplyproductions.economy.main.Economy;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class EconomyAPI {

    private OfflinePlayer player;

    /**
     * Create a new instance of type {@link EconomyAPI}.
     *
     * @param player the player you are going to manage.
     */
    public EconomyAPI(OfflinePlayer player) {
        this.player = player;
    }

    /**
     * Get the current player.
     *
     * @return the current player.
     */
    public OfflinePlayer getPlayer() {
        return player;
    }

    /**
     * Change the player.
     *
     * @param player the new player.
     */
    public void setPlayer(OfflinePlayer player) {
        this.player = player;
    }

    /**
     * Get the file of the player.
     *
     * @return the file.
     */
    private FileBuilder getFile() {
        FileBuilder file = new FileBuilder(Economy.getInstance().getDataFolder() + "//Users//" + player.getUniqueId().toString(), "account.yml");

        if (!file.exists()) {
            file.create();

            ConfigEditor config = file.getConfig();

            config.set("Balance", Configuration.get(Configuration.START_AMOUNT.getPath()));
        }
        return file;
    }

    /**
     * Get the player's current balance.
     *
     * @return the current balance.
     */
    public Integer get() {
        return (Integer) getFile().getConfig().get("Balance");
    }

    /**
     * Get the player's current balance.
     *
     * @return the current balance.
     */
    public boolean has(Integer amount) {
        return get() >= amount;
    }

    /**
     * Get the missing balance until the amount has reached.
     *
     * @return the missing balance.
     */
    public Integer missing(Integer amount) {
        return amount - get();
    }

    /**
     * Set the balance of the player's bank account.
     *
     * @param amount the amount.
     * @return if the process was successful.
     */
    public boolean set(Integer amount) {
        boolean success = true;
        try {
            FileBuilder file = getFile();
            ConfigEditor config = file.getConfig();

            config.set("Balance", amount);

        } catch (Exception e) {
            success = false;
        }
        return success;
    }

    /**
     * Add the given amount to the player's balance.
     *
     * @param amount the amount.
     * @return the player's new balance. (error: -1)
     */
    public Integer add(Integer amount, Transaction transaction) {
        BalanceChangeEvent event = new BalanceChangeEvent(player, amount, get(), get() + amount, transaction);
        Bukkit.getPluginManager().callEvent(event);

        set(get() + amount);

        if (transaction != null) {
            TransactionAPI tapi = new TransactionAPI(player);
            tapi.add(transaction);
        }

        return get();
    }

    /**
     * Remove the given amount from the player's balance.
     *
     * @param amount      the amount.
     * @param transaction the transaction.
     * @return the player's new balance. (error: -1)
     */
    public Integer remove(Integer amount, Transaction transaction) {
        if (amount <= get()) {
            BalanceChangeEvent event = new BalanceChangeEvent(player, amount, get(), get() - amount, transaction);
            Bukkit.getPluginManager().callEvent(event);

            if (transaction != null) {
                TransactionAPI tapi = new TransactionAPI(player);
                tapi.add(transaction);
            }

            set(get() - amount);
        }
        return -1;
    }

    /**
     * Get the right Currency-Name for the given balance.
     * eg. 1 "DOLLAR" or 2 "DOLLARS"
     *
     * @param balance the balance.
     * @return the currency-name.
     */
    public static String getCurrencyName(Integer balance) {
        String singular = String.valueOf(Configuration.get(Configuration.CURRENCY_NAME_SINGULAR.getPath()));
        String plural = String.valueOf(Configuration.get(Configuration.CURRENCY_NAME_PLURAL.getPath()));

        return (balance == 1 ? singular : plural);
    }

    /**
     * Get the balance formatted with thousand seperator.
     * eg. 1.000.000
     *
     * @param balance the balance.
     * @return the seperated balance (string).
     */
    public static String getSeperatedBalance(int balance) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

        char c = String.valueOf(Configuration.get(Configuration.CURRENCY_THOUSAND_SEPERATOR.getPath())).charAt(0);
        symbols.setGroupingSeparator(c);
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(balance);
    }

    /**
     * Get a perfect string for messages.
     * eg. 1.000.000 Dollars or 1 Dollar.
     *
     * @param balance the balance.
     * @return the perfect string.
     */
    public static String getString(int balance) {
        return getSeperatedBalance(balance) + " " + getCurrencyName(balance);
    }

}
