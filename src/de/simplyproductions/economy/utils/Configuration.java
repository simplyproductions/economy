package de.simplyproductions.economy.utils;

import de.simplyproductions.economy.main.Economy;

import java.util.ArrayList;
import java.util.List;

public enum Configuration {

    PREFIX("Chat.Prefix", "&8[&6Money&8] &7"),
    START_AMOUNT("Currency.StartAmount", 0),
    CURRENCY_NAME_SINGULAR("Currency.Name.Singular", "Dollar"),
    CURRENCY_NAME_PLURAL("Currency.Name.Plural", "Dollars"),
    CURRENCY_THOUSAND_SEPERATOR("Currency.ThousandSeperator", '.'),
    TRANSACTION_LOG_CONSOLE("Transactions.Logging.Console", true);

    private String path;
    private Object standardValue;

    Configuration(String path, Object standardValue) {
        this.path = path;
        this.standardValue = standardValue;
    }

    public String getPath() {
        return path;
    }

    public Object getStandardValue() {
        return standardValue;
    }

    private static FileBuilder getFile() {
        return new FileBuilder(Economy.getInstance().getDataFolder().getPath(), "config.yml");
    }

    public static List<Configuration> setup() {
        List<Configuration> written = new ArrayList<>();
        FileBuilder file = getFile();

        if (!file.exists()) {
            file.create();
        }

        ConfigEditor config = file.getConfig();

        for (Configuration cfg : values()) {
            if (!config.contains(cfg.getPath())) {
                config.set(cfg.getPath(), cfg.getStandardValue());
                written.add(cfg);
            }
        }
        return written;
    }

    public static Object get(String path) {
        return getFile().getConfig().get(path);
    }

    public static void set(String path, Object value) {
        getFile().getConfig().set(path, value);
    }

}
