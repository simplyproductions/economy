package de.simplyproductions.economy.utils;

import de.simplyproductions.economy.main.Economy;
import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.List;

public class TransactionAPI {

    private OfflinePlayer player;

    /**
     * Create a new instance of type {@link TransactionAPI}.
     *
     * @param player the player you are going to manage.
     */
    public TransactionAPI(OfflinePlayer player) {
        this.player = player;
    }

    /**
     * Get the current player.
     *
     * @return the current player.
     */
    public OfflinePlayer getPlayer() {
        return player;
    }

    /**
     * Change the player.
     *
     * @param player the new player.
     */
    public void setPlayer(OfflinePlayer player) {
        this.player = player;
    }

    /**
     * Get the file of the player.
     *
     * @return the file.
     */
    private FileBuilder getFile() {
        FileBuilder file = new FileBuilder(Economy.getInstance().getDataFolder() + "//Users//" + player.getUniqueId().toString(), "transactions.yml");

        if (!file.exists()) {
            file.create();

            ConfigEditor config = file.getConfig();

            config.set("TransactionLog", new ArrayList<Transaction>());
        }
        return file;
    }

    /**
     * Get the file of the player.
     *
     * @return the file.
     */
    public static FileBuilder getFile(OfflinePlayer player) {
        FileBuilder file = new FileBuilder(Economy.getInstance().getDataFolder() + "//Users//" + player.getUniqueId().toString(), "transactions.yml");

        if (!file.exists()) {
            file.create();

            ConfigEditor config = file.getConfig();

            config.set("TransactionLog", new ArrayList<Transaction>());
        }
        return file;
    }

    /**
     * add a transaction to the player's log-file.
     *
     * @param transaction the transaction.
     */
    public void add(Transaction transaction) {
        FileBuilder file = getFile();
        ConfigEditor config = file.getConfig();
        List<String> transactions = (List<String>) config.get("TransactionLog");

        transactions.add(transaction.toString());

        config.set("TransactionLog", transactions);
    }

    /**
     * Get a list of all transactions of the player that are documentated (as strings).
     *
     * @return list of all documented transactions (as strings).
     */
    public List<String> getTransactionStrings() {
        return (List<String>) getFile(player).getConfig().get("TransactionLog");
    }

    /**
     * Get a list of all transactions of the player that are documentated.
     *
     * @return list of all documented transactions.
     */
    public List<Transaction> getTransactions() {
        List<Transaction> transactions = new ArrayList<>();

        for(String transactionString : getTransactionStrings()) {
            Transaction transaction = Transaction.fromString(transactionString);
            transactions.add(transaction);
        }
        return transactions;
    }

}
