package de.simplyproductions.economy.utils;

import de.simplyproductions.economy.utils.transactions.types.TransactionType;
import de.simplyproductions.economy.utils.transactions.types.TransactionTypeManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.Date;
import java.util.UUID;

public class Transaction {

    private OfflinePlayer sender, recipient;
    private Integer amount;
    private TransactionType type;
    private Date date;

    /**
     * Create a new instance of type {@link Transaction}.
     *
     * @param sender the player whom will be charged/favoured.
     * @param amount the amount of the transaction.
     * @param type   the type of this transaction
     * @param date   the timestamp of this transaction.
     */
    public Transaction(OfflinePlayer sender, OfflinePlayer recipient, Integer amount, TransactionType type, Date date) {
        this.sender = sender;
        this.recipient = recipient;
        this.amount = amount;
        this.type = type;
        this.date = date;
    }

    /**
     * Get the sender of this transaction-instance.
     *
     * @return the sender.
     */
    public OfflinePlayer getSender() {
        return sender;
    }

    /**
     * Get the recipient of this transaction-instance.
     *
     * @return the recipient.
     */
    public OfflinePlayer getRecipient() {
        return recipient;
    }

    /**
     * Get the amount of this transaction-instance.
     *
     * @return the amount.
     */
    public Integer getAmount() {
        return amount;
    }

    /**
     * Get the {@link TransactionType} of this transaction-instance.
     *
     * @return the {@link TransactionType}.
     */
    public TransactionType getType() {
        return type;
    }

    /**
     * Set the {@link TransactionType} of this transaction-instance.
     *
     * @param type the type.
     */
    public void setType(TransactionType type) {
        this.type = type;
    }


    /**
     * Get the timestamp of this transaction-instance.
     *
     * @return the timestamp.
     */
    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return this.sender.getUniqueId().toString() + "##" + (this.recipient != null ? this.recipient.getUniqueId().toString() : "NONE") + "##" + this.amount + "##" + this.date.getTime() + "##" + type.getName().toUpperCase();
    }

    /**
     * Convert a log-string to an instance of type {@link Transaction}.
     *
     * @param transactionString
     * @return
     */
    public static Transaction fromString(String transactionString) {
        OfflinePlayer sender = Bukkit.getOfflinePlayer(UUID.fromString(transactionString.split("##")[0]));
        OfflinePlayer recipient = null;
        String[] splitted = transactionString.split("##");

        if (!transactionString.split("##")[1].equalsIgnoreCase("NONE")) {
            recipient = Bukkit.getOfflinePlayer(UUID.fromString(splitted[0]));
        }

        int amount = Integer.parseInt(splitted[2]);
        Long time = Long.valueOf(splitted[3]);
        Date date = new Date(time);
        TransactionType type = TransactionTypeManager.getByName(splitted[4]);

        return new Transaction(sender, recipient, amount, type, date);
    }

}
