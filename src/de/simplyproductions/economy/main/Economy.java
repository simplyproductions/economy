package de.simplyproductions.economy.main;

import de.simplyproductions.economy.commands.Command_Economy;
import de.simplyproductions.economy.commands.Command_Helper;
import de.simplyproductions.economy.listeners.Listener_DebugListeners;
import de.simplyproductions.economy.listeners.custom.Listener_CallCustomEvents;
import de.simplyproductions.economy.listeners.inventories.Listener_MainWalletInventory;
import de.simplyproductions.economy.listeners.inventories.Listener_PaymentInventory;
import de.simplyproductions.economy.utils.Configuration;
import de.simplyproductions.economy.utils.transactions.types.TransactionTypeManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;
import de.simplyproductions.economy.commands.Command_Money;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class Economy extends JavaPlugin {

    private static Plugin plugin;

    @Override
    public void onEnable() {
        plugin = this;

        TransactionTypeManager.registerTypes();

        List<Configuration> written_ConfigSections = Configuration.setup();
        String prefix = ChatColor.translateAlternateColorCodes('&', String.valueOf(Configuration.get(Configuration.PREFIX.getPath())));

        if (written_ConfigSections.size() >= 1) {
            System.out.println(prefix + "Added " + written_ConfigSections.size() + " " + (written_ConfigSections.size() == 1 ? "entry" : "entries") + " to the configuration:");

            for (Configuration cfg : written_ConfigSections) {
                System.out.println("-> " + cfg.getPath() + ": " + Configuration.get(cfg.getPath()));
            }
        } else {
            System.out.println(prefix + "There were no new config entries added.");
        }


        loadCommands();
        loadEvents();
    }

    @Override
    public void onDisable() {

    }

    public static Plugin getInstance() {
        return plugin;
    }

    private void loadCommands() {
        getCommand("money").setExecutor(new Command_Money());
        getCommand("economy").setExecutor(new Command_Economy());
        getCommand("helper").setExecutor(new Command_Helper());
    }

    private void loadEvents() {
        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new Listener_CallCustomEvents(), this);

        pm.registerEvents(new Listener_DebugListeners(), this);

        pm.registerEvents(new Listener_MainWalletInventory(), this);
        pm.registerEvents(new Listener_PaymentInventory(), this);
    }

}
