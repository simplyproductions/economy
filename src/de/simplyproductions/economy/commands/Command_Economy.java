package de.simplyproductions.economy.commands;

import de.simplyproductions.economy.utils.EconomyAPI;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Command_Economy implements CommandExecutor {
    @SuppressWarnings("deprecation")
    @Override
    public boolean onCommand(CommandSender cs, Command command, String s, String[] args) {
        if (args.length == 3) {
            OfflinePlayer op = Bukkit.getOfflinePlayer(args[1]);
            EconomyAPI eco = new EconomyAPI(op);

            try {
                Integer amount = Integer.parseInt(args[2]);

                if (args[0].equalsIgnoreCase("set")) {
                    eco.set(amount);
                    cs.sendMessage("§7The Balance from §2" + op.getName() + " §7was set to §a" + EconomyAPI.getString(amount) + "§7.");
                } else if (args[0].equalsIgnoreCase("add")) {
                    eco.add(amount, null);
                    cs.sendMessage("§7The Balance from §2" + op.getName() + " §7was increased by §a" + EconomyAPI.getString(amount) + "§7.");
                } else if (args[0].equalsIgnoreCase("remove")) {
                    eco.remove(amount, null);
                    cs.sendMessage("§7The Balance from §2" + op.getName() + " §7was decreased by §a" + EconomyAPI.getString(amount) + "§7.");
                } else {
                    sendUsage(cs);
                }
            } catch (NumberFormatException e) {
                sendUsage(cs);
            }
        } else {
            sendUsage(cs);
        }
        return true;
    }

    public void sendUsage(CommandSender cs) {
        cs.sendMessage("§2The correct format is /economy [add,set,remove] [Player] [Amount]");
    }

}
