package de.simplyproductions.economy.commands;

import de.simplyproductions.economy.utils.EconomyAPI;
import de.simplyproductions.economy.utils.Wallet;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;


public class Command_Money implements CommandExecutor {

    @SuppressWarnings("deprecation")
    @Override
    public boolean onCommand(CommandSender cs, Command command, String s, String[] args) {
        if (args.length == 0) {
            if (cs instanceof Player) {
                Player p = (Player) cs;
                Wallet wallet = new Wallet();
                Inventory inv = wallet.getMainInventory(p, p);

                p.openInventory(inv);
            }
        } else if (args.length == 1) {
            OfflinePlayer off_player = Bukkit.getOfflinePlayer(args[0]);

            if (cs instanceof Player) {
                Player p = (Player) cs;
                Wallet wallet = new Wallet();
                Inventory inv = wallet.getMainInventory(p, off_player);

                p.openInventory(inv);
            } else {
                EconomyAPI eco = new EconomyAPI(off_player);

                cs.sendMessage("§2The Account balance of §a" + off_player.getName() + " §2is §a" + EconomyAPI.getString(eco.get()) + "§2.");
            }
        } else {
            cs.sendMessage("§c/" + s + " [Player]");
        }
        return true;
    }

    /* -------------------------------------------------

       Moved getInventory and getGlass to Wallet.java

     -------------------------------------------------*/

}
