package de.simplyproductions.economy.listeners;

import de.simplyproductions.economy.listeners.custom.BalanceChangeEvent;
import de.simplyproductions.economy.utils.Configuration;
import de.simplyproductions.economy.utils.Transaction;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class Listener_DebugListeners implements Listener {

    @EventHandler
    public void balanceChange(BalanceChangeEvent event) {
        OfflinePlayer op = event.getPlayer();
        Transaction transaction = event.getTransaction();

        if ((boolean) Configuration.get(Configuration.TRANSACTION_LOG_CONSOLE.getPath())) {
            System.out.println("---------------------------------------------------------------------------");
            System.out.println("NEW TRANSACTION:");
            System.out.println();
            System.out.println();
            System.out.println("Sender: " + transaction.getSender().getName());
            System.out.println("Recipient: " + transaction.getRecipient().getName());
            System.out.println("Amount: " + transaction.getAmount());
            System.out.println("Type: " + transaction.getType().getDisplayName() + " (" + transaction.getType().getName() + ")");
            System.out.println("Timestamp: " + transaction.getDate().toString());
            System.out.println("---------------------------------------------------------------------------");
        }
    }

}
