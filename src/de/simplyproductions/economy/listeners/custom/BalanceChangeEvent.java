package de.simplyproductions.economy.listeners.custom;

import de.simplyproductions.economy.utils.Transaction;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * This event gets fired everytime the balance of a bank
 * account of a player changes. No matter if positive or negative.
 *
 */
public class BalanceChangeEvent extends Event {

    public static HandlerList handlers = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    private OfflinePlayer player;
    private Integer amount, before, after;
    private Transaction transaction;

    /**
     * Create a new instance of this event.
     *
     * @param player the player whom's account balance has changed.
     * @param amount the amount by which the account balance has changed.
     * @param transaction the connected transaction from this event.
     */
    public BalanceChangeEvent(OfflinePlayer player, Integer amount, Integer before, Integer after, Transaction transaction) {
        this.player = player;
        this.amount = amount;
        this.before = before;
        this.after = after;
        this.transaction = transaction;
    }

    /**
     * Get the player whom's account balance has changed.
     *
     * @return the player.
     */
    public OfflinePlayer getPlayer() {
        return player;
    }

    /**
     * Get the amount by which the account balance has changed.
     *
     * @return the amount.
     */
    public Integer getAmount() {
        return amount;
    }

    /**
     * Get the amount of money the player had before this transaction.
     *
     * @return the amount of money.
     */
    public Integer getBefore() {
        return before;
    }

    /**
     * Get the amount of money the player has now.
     *
     * @return the amount of money.
     */
    public Integer getAfter() {
        return after;
    }

    /**
     * Get the connected transaction from this event.
     *
     * @return the transaction.
     */
    public Transaction getTransaction() {
        return transaction;
    }

}
