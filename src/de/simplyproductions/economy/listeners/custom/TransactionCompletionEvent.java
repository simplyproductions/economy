package de.simplyproductions.economy.listeners.custom;

import de.simplyproductions.economy.utils.Transaction;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This event gets fired everytime a transaction is completed
 * by any player.
 */
public class TransactionCompletionEvent extends Event {

    public static HandlerList handlers = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    private OfflinePlayer executor;
    private Transaction transaction;

    /**
     * Create a new instance of this event.
     *
     * @param executor the player whom has executed the transaction.
     * @param transaction the transaction.
     */
    public TransactionCompletionEvent(OfflinePlayer executor, Transaction transaction) {
        this.executor = executor;
        this.transaction = transaction;
    }

    /**
     * Get the executing player.
     *
     * @return the player.
     */
    public OfflinePlayer getExecutor() {
        return executor;
    }

    /**
     * Get the transaction.
     *
     * @return the transaction.
     */
    public Transaction getTransaction() {
        return transaction;
    }

}
