package de.simplyproductions.economy.listeners.inventories;

import de.simplyproductions.economy.utils.Wallet;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class Listener_MainWalletInventory implements Listener {

    @EventHandler
    public void mainMenuClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        Wallet wallet = new Wallet();

        if (e.getInventory() != null) {
            if (e.getInventory().getTitle().equals(new Wallet().getMainInventory(p, p).getTitle())) {
                e.setCancelled(true);

                if (e.getCurrentItem().isSimilar(Wallet.MainInventoryContent.getPaymentIcon().getItemStack())) {
                    p.openInventory(wallet.getPaymentInventory(p));
                }
            }
        }
    }

}
