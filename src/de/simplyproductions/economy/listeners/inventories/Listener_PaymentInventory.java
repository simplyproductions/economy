package de.simplyproductions.economy.listeners.inventories;

import de.simplyproductions.economy.utils.*;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class Listener_PaymentInventory implements Listener {

    @EventHandler
    public void mainMenuClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        Wallet wallet = new Wallet();
        Payment payment = (Payment.getActivePayment(p) != null ? Payment.getActivePayment(p) : new Payment(p));
        Payment.payments.put(p, payment);

        if (e.getInventory() != null) {
            if (e.getInventory().getTitle().equals(new Wallet().getPaymentInventory(p).getTitle())) {
                e.setCancelled(true);

                if (e.getSlot() == ChestSlot.getSlot(2, 3)) {
                    // PLAYER CHOOSING
                    wallet.openPlayerSelection(p, 1);
                } else if (e.getSlot() == ChestSlot.getSlot(2, 5) && e.getCurrentItem().getType() != Material.BARRIER) {
                    // AMOUNT CHOOSING
                    wallet.openAmountSelection(p);
                } else if (e.getSlot() == ChestSlot.getSlot(2, 7) && e.getCurrentItem().getType() != Material.BARRIER) {
                    // FINISHING PAYMENT
                    payment = Payment.getActivePayment(p);

                    if (payment != null) {
                        Payment.CancellationCause ccause = payment.pay();

                        if (ccause != null) {
                            switch (ccause) {
                                case NO_RECIPIENT:
                                    p.sendMessage("§cThere isn't a recipient set.");
                                    break;

                                case NO_AMOUNT:
                                    p.sendMessage("§cThere isn't an amount set.");
                                    break;

                                case MISSING_BALANCE:
                                    p.sendMessage("§cYou don't have enough money for this payment.");
                                    break;
                            }
                        } else {
                            p.sendMessage("§aTransaction proceeded successfully.");
                            p.playSound(p.getLocation(), Sound.LEVEL_UP, 2, 2);
                        }
                    } else {
                        p.sendMessage("§cThere was a issue with your transaction! Please try again soon.");
                    }

                    p.closeInventory();
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void playerSelection(InventoryClickEvent event) {
        Player p = (Player) event.getWhoClicked();
        Wallet wallet = new Wallet();

        if (event.getView().getTitle().equalsIgnoreCase("§aChoose a recipient")) {
            event.setCancelled(true);
            Payment payment = (Payment.getActivePayment(p) != null ? Payment.getActivePayment(p) : new Payment(p));
            Payment.payments.put(p, payment);

            if (event.getCurrentItem() != null) {
                if (event.getCurrentItem().getType() == Material.SKULL_ITEM) {
                    OfflinePlayer recipient = Bukkit.getOfflinePlayer(ChatColor.stripColor(new Item(event.getCurrentItem()).getName()));

                    payment.setRecipient(recipient);
                    Payment.payments.put(p, payment);
                    p.playSound(p.getLocation(), Sound.VILLAGER_YES, 2, 1);
                    p.openInventory(wallet.getPaymentInventory(p));
                } else if (event.getCurrentItem().isSimilar(wallet.getBack().getItemStack())) {
                    p.openInventory(wallet.getPaymentInventory(p));
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void amountSelection(InventoryClickEvent event) {
        Player p = (Player) event.getWhoClicked();
        Wallet wallet = new Wallet();

        if (event.getView().getTitle().equalsIgnoreCase("§aChoose an amount")) {
            event.setCancelled(true);
            Payment payment = (Payment.getActivePayment(p) != null ? Payment.getActivePayment(p) : new Payment(p));
            Payment.payments.put(p, payment);

            if (event.getCurrentItem() != null) {
                if (event.getCurrentItem().isSimilar(wallet.getAmountMinusOneIcon().getItemStack())) {
                    payment.setAmount(payment.getAmount() - 1);
                } else if (event.getCurrentItem().isSimilar(wallet.getAmountMinusTenIcon().getItemStack())) {
                    payment.setAmount(payment.getAmount() - 10);
                } else if (event.getCurrentItem().isSimilar(wallet.getAmountMinusHundredIcon().getItemStack())) {
                    payment.setAmount(payment.getAmount() - 100);
                } else if (event.getCurrentItem().isSimilar(wallet.getAmountMinusThousandIcon().getItemStack())) {
                    payment.setAmount(payment.getAmount() - 1000);
                } else if (event.getCurrentItem().isSimilar(wallet.getAmountPlusOneIcon().getItemStack())) {
                    payment.setAmount(payment.getAmount() + 1);
                } else if (event.getCurrentItem().isSimilar(wallet.getAmountPlusTenIcon().getItemStack())) {
                    payment.setAmount(payment.getAmount() + 10);
                } else if (event.getCurrentItem().isSimilar(wallet.getAmountPlusHundredIcon().getItemStack())) {
                    payment.setAmount(payment.getAmount() + 100);
                } else if (event.getCurrentItem().isSimilar(wallet.getAmountPlusThousandIcon().getItemStack())) {
                    payment.setAmount(payment.getAmount() + 1000);
                } else if (event.getCurrentItem().isSimilar(wallet.getAmountSaveIcon().getItemStack())) {
                    Payment.payments.put(p, payment);
                    p.openInventory(wallet.getPaymentInventory(p));
                }

                if (!event.getCurrentItem().isSimilar(wallet.getAmountSaveIcon().getItemStack())) {
                    Payment.payments.put(p, payment);
                    wallet.openAmountSelection(p);
                }
            }
        }
    }

}
